#include "TM4C123GH6PM.h"

#include "stdint.h"

#define INT_32_63_ENABLE  (*((volatile unsigned long *)0xE000E104))
/**
 * main.c
 */
volatile uint32_t adcresult = 0;

int main(void)
{
    /*** PE1 - AN1 ***/

   /*** Module initialization ***/


    /** Enable the ADC clock using RCGADC register **/
    SYSCTL->RCGCADC |= (1<<1);

    /*** Clock gating for GPIO port E( for ADC Channel)***/
    SYSCTL->RCGCGPIO |= (1<<4);

    // set direction as input
    GPIOE->DIR &= ~(1<<1);

    //select alternate function for port E which is connected to ADC
    GPIOE->AFSEL |= (1<<1);

    //disable digital enable for portE
    GPIOE->DEN &= ~(1<<1);

/* 1 - The analog function of the pin is enabled, the isolation is
disabled, and the pin is capable of analog functions.  */
    GPIOE->AMSEL |= (1<<1);

    /**  Diable the sequencer during configuration - Here we are using sequencer 3**/
    ADC1->ACTSS &= ~(1<<3);

    /** Configure the trigger event for the sample sequencer - 0xF Always (continuously sample for sequencer 3 15:12 bits to configure tigger options**/
    ADC1->EMUX |= (0xf<<12);

    /** configure the corresponding input source - here we use PE1 **/
    ADC1->SSMUX3 |= 0x02;

    /** 2nd bit for interrupt control 1st bit for end sequence bit (must be enabled) **/
    ADC1->SSCTL3 |= 0x06;  // 0x06 - 110

    /** The raw interrupt signal from Sample Sequencer 3 (ADCRIS
        register INR3 bit) is sent to the interrupt controller **/
    ADC1->IM |= (1<<3);

    /** enable the sample sequencer **/
    ADC1->ACTSS |= (1<<3);

    /**  This bit is cleared by writing a 1 to it. Clearing this bit also clears the
        INRDC bit in the ADCRIS register. **/
    ADC1->ISC |= (1<<3);

    /** Enable NVIC ADC interrupt **/
    NVIC->ISER[1] |= (1<<19);

    /** configure LED Pin **/
    SYSCTL->RCGC2 |= (1<<5);
    GPIOF->DEN |= 0x02;
    GPIOF->DIR |= 0x02;

    while(1)
    {
        if(adcresult >= 4000)
        {
            GPIOF->DATA |= (1<<1);  // enable red led
            ADC1->IM |= (1<<3);
        }
        else
        {
            GPIOF->DATA &= ~(1<<1); // disable red led
            ADC1->IM |= (1<<3);
        }
    }

}
